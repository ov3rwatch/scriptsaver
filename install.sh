#!/bin/bash

#Set the function to make the animated printout:
function typewriter
{
    text="$1"

    for i in $(seq 0 $(expr length "${text}")) ; do
        echo -n "${text:$i:1}"
        sleep 0.0001 # This value sets the speed of the animation anything over 0.5 is ungodly slow. Just an fyi
    done
}

clear

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi
eula=`cat eula.txt`
echo "===================== Installing Scriptsaver ====================="
echo "| Scriptsaver was written by: JL Griffin                         |"
echo "| Released into public domain (no restrictions)                  |"
echo "|                                                                |"
echo "| Befor Proceding you must accept the EULA:                      |"
echo "|    ________________________________________________________    |"
echo "|                                                                |"
typewriter "$eula"
echo
echo -n "| Do You Agree? (Y/n): "
read yn
if [ "$yn" = "n" ]; then
    echo "|Exiting...                                                      |"
    echo "|________________________________________________________________|"
    sleep 5
    exit 1
fi 
if [ "$yn" = "N" ]; then
    echo "|Exiting...                                                      |"
    echo "|________________________________________________________________|"
    sleep 5
    exit 1
fi 

echo "| You have agreed to the terms. Install will begin now...        |"
echo "|________________________________________________________________|"
echo " "
echo " "
echo "Starting Install..."
echo -n "Creating the install directory."
sleep 1
echo -n "."
sleep 1
echo -n "."
mkdir /usr/src/scriptsaver 2>/dev/null
echo "       [OK]"
echo -n "Copying source files."
sleep 1
cp ./daemon-runner.sh /usr/src/scriptsaver/daemon-runner.sh 2>/dev/null
sleep 1
echo -n "."
cp ./runner.sh /usr/src/scriptsaver/runner.sh 2>/dev/null
sleep 1
echo -n "."
#cp ./daemon-dtop.sh /usr/src/scriptsaver/daemon-dtop.sh
sleep 1
echo -n "."
#cp ./daemon-ssav.sh /usr/src/scriptsaver/daemon-ssav.sh
sleep 1
echo -n "."
echo "               [OK]"
echo -n "Applying Permissions."
sleep 1
echo -n "."
chmod -R 555 /usr/src/scriptsaver/
sleep 1
echo -n "."
sleep 1
echo "                 [OK]"
echo -n "Processing Triggers: |"
sleep 1
echo -n "====="
sleep 1
echo -n "====="
sleep 1
echo -n "====="
sleep 1
echo "======|"
sleep 2
echo -n "Running Daemon..."
sleep 2 
echo "                       [OK]"
echo "== Install Complete! =="
echo
sleep 1