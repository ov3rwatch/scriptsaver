#!/bin/bash

#Set the function to make the animated printout:
function typewriter
{
    text="$1"

    for i in $(seq 0 $(expr length "${text}")) ; do
        echo -n "${text:$i:1}"
        sleep 0.008 # This value sets the speed of the animation anything over 0.5 is ungodly slow. Just an fyi
    done
}

#Clear the screen for the initial script to begin running
clear

#Set the loop variable
start=runnerX

#Start the loop
while [ "$start" = "runnerX" ]
do

#Set the draw-from directory where the script should grab a random file from.
dir='/usr/src/linux/kernel'

#Grab a random file from the draw-from directory
file=`/bin/ls -1 "$dir" | sort --random-sort | head -1`

#turn the selected file into a full path
path=`readlink --canonicalize "$dir/$file"` # Converts to full path/

#the following version of the string variable actually uses the randomly
#selected file we picked in the code above. if your not testing a change
#leave this line uncommented otherwise it will only echo a test string.
string=`cat $path` 

#TESTING MODE Comment the line above and uncomment echo the testing string 
#below instead of the randomly selected file [NOT RECOMMENDED UNLESS MAKING CODE CHANGES]

#string="
#=================Code Testing Mode=================================================
#| The draw-from directory is: $dir
#| The randomly selected file is: $file
#| The Final Output is: $path 
#| And now the Trailing echo statements will run.
#==================================================================================="


#the following line is the actual animation of text echoing printing out 
#character by character as if being typed. makes a very hollywood kind of
#feel that looks really neat :D
typewriter "$string"
echo # <-- This makes a new line. The ouput gets skewed without this.

#This part is just here to look cool and has absolutely no function whatsoever.
#It is just a bunch of echo statements and sleep values to make it look like 
#this script is doing something far more productive than it actually is. :D
echo ""
echo ""
echo "Compiling $path"
echo -n "Please wait"
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo -n "."
sleep 1
echo  "."
echo -n "Compiled!"
sleep 2
echo "                      [OK]"
sleep 1
echo -n "Creating Next file..."
sleep 2              
echo "          [OK]"
sleep 1
echo -n "Starting Scripting Session"
sleep 2
echo "     [OK]"
sleep 3
echo " "
echo " "
#If you would not like the screen to clear with each newly loaded file comment
#the following line.
clear
#End loop
done