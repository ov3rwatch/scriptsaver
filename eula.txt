|                                                                |
|END USER LICENSE AGREEMENT                                      |
|                                                                |
|                                                                |
|This copy of ScriptSaver ("the Software Product") and           |
|accompanying documentation is open source and not sold.         |
|                                                                |
|Acceptance                                                      |
|YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS AGREEMENT |
|BY SELECTING THE "ACCEPT" OPTION AND DOWNLOADING THE SOFTWARE   |
|PRODUCT OR BY INSTALLING, USING, OR COPYING THE SOFTWARE PRODUCT| 
|YOU MUST AGREE TO ALL OF THE TERMS OF THIS AGREEMENT BEFORE YOU |
|WILL BE ALLOWED TO DOWNLOAD THE SOFTWARE PRODUCT. IF YOU DO NOT |
|AGREE TO ALL OF THE TERMS OF THIS AGREEMENT, YOU MUST SELECT    |
|"DECLINE" AND YOU MUST NOT INSTALL, USE, OR COPY THE SOFTWARE   |
|PRODUCT.                                                        |
|                                                                |
|You may modify the Software Product or create any derivative    |
|work of the Software Product or its accompanying documentation. |
|Derivative works include but are not limited to translations.   |
|You may alter any files or libraries in any portion of the      |
|Software Product.                                               |
|                                                                |
|Disclaimer of Warranties and Limitation of Liability            |
|UNLESS OTHERWISE EXPLICITLY AGREED TO IN WRITING BY OV3RWATCH,  |
|OV3RWATCH MAKES NO OTHER WARRANTIES, EXPRESS OR IMPLIED, IN FACT|
|OR IN LAW, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES|
|OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OTHER    |
|THAN AS SET FORTH IN THIS AGREEMENT OR IN THE LIMITED WARRANTY  |
|DOCUMENTS PROVIDED WITH THE SOFTWARE PRODUCT.                   |
|                                                                |
|ov3rwatch makes no warranty that the Software Product will meet |
|your requirements or operate under your specific conditions of  |
|use. ov3rwatch makes no warranty that operation of the Software |
|Product will be secure, error free, or free from interruption.  |
|YOU MUST DETERMINE WHETHER THE SOFTWARE PRODUCT SUFFICIENTLY    |
|MEETS YOUR REQUIREMENTS FOR SECURITY AND UNINTERRUPTABILITY. YOU|
|BEAR SOLE RESPONSIBILITY AND ALL LIABILITY FOR ANY LOSS INCURRED|
|DUE TO FAILURE OF THE SOFTWARE PRODUCT TO MEET YOUR REQUIREMENTS|
|OV3RWATCH WILL NOT, UNDER ANY CIRCUMSTANCES, BE RESPONSIBLE OR  |
|LIABLE FOR THE LOSS OF DATA ON ANY COMPUTER OR INFORMATION      |
|STORAGE DEVICE.                                                 |
|                                                                |
|UNDER NO CIRCUMSTANCES SHALL OV3RWATCH, ITS PLANNERS, CODERS,   |
|PROGRAMMERS OR HACKERS BE LIABLE TO YOU OR ANY OTHER PARTY FOR  |
|INDIRECT, CONSEQUENTIAL, SPECIAL, INCIDENTAL, PUNITIVE, OR      |
|EXEMPLARY DAMAGES OF ANY KIND (INCLUDING LOST REVENUES OR       |
|PROFITS OR LOSS OF BUSINESS) RESULTING FROM THIS AGREEMENT, OR  |
|FROM THE FURNISHING, PERFORMANCE, INSTALLATION, OR USE OF THE   |
|SOFTWARE PRODUCT, WHETHER DUE TO A BREACH OF CONTRACT, BREACH OF|
|WARRANTY, OR THE NEGLIGENCE OF OV3RWATCH OR ANY OTHER PARTY,    |
|EVEN IF OV3RWATCH IS ADVISED BEFOREHAND OF THE POSSIBILITY OF   |
|SUCH DAMAGES. TO THE EXTENT THAT THE APPLICABLE JURISDICTION    |
|LIMITS OV3RWATCH'S ABILITY TO DISCLAIM ANY IMPLIED WARRANTIES,  |
|THIS DISCLAIMER SHALL BE EFFECTIVE TO THE MAXIMUM EXTENT        |
|PERMITTED.                                                      |
|                                                                |
|Governing Law, Jurisdiction and Costs                           |
|This Agreement is governed by the laws of Illinois, without     |
|regard to Illinois's conflict or choice of law provisions.      |
|                                                                |